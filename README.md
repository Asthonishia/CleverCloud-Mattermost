# Mattermost on Clever Cloud deployment script

The script here allows the deployment of a [Mattermost](https://mattermost.com/) docker instance at the host [Clever Cloud](https://www.clever-cloud.com).

## Getting Started

### Prerequisites

Minimum requirements :

```
App : Docker Instance S minimum ( 2 CPUs, 2048 MB )
PostgreSQL Addon : Plan S minimum ( Memory: shared, Max connection limit: 10, Max DB size: 1Gb, vCPUs: shared )
Amazon S3 Add-on : Plan S 
```

### Installing

Configure the following environment variables on your docker instance :

```
CELLAR_ADDON_BUCKET
SITE_NAME
SITE_URL
SMTP_HOST
SMTP_HOST_PORT
SMTP_USER
SMTP_PASSWORD
MAIL_FEEDBACK_EMAIL
MAIL_FEEDBACK_NAME
```

Just push the branch on Clever Cloud App instance.

## Authors

* **Nicolas TESSIER** - *Project Manager / IT Developer & Designer* - [Oniti](https://www.oniti.fr)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
